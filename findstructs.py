import re, os, traceback

regularexpression = "struct \w{1,}\ *"
varlist = []
varfile = open("gdbvars.output")
s = varfile.read()
varlist = re.findall(regularexpression, s)
varlist = list(dict.fromkeys(varlist))
print(varlist)
varfile.close()

#create new gdb batch file
newbatchfile = open("gdbbatchcmd", "w")
newbatchfile.write("define findsizes\n")
newbatchfile.write("\tb 29\n")
newbatchfile.write("\trun\n")
newbatchfile.write("\tset pagination off\n")
newbatchfile.write("\tset logging file gdbsizes.output\n")
newbatchfile.write("\tset logging on\n")

for i in range(len(varlist)):
    newbatchfile.write("\tprint sizeof("+varlist[i]+")\n")

newbatchfile.close()

output = os.system("gdb -ex \"source gdbbatchcmd\" -ex \"findsizes\" ./a.out  -quiet")

sizelist =  []
sizefile = open("gdbsizes.output", "r")
newline = " "
returned = ""
while(len(newline)!=0):
    returned = newfile.readline()
    returned = returned.split("= ")[1]
    sizelist.append(returned)

print(sizelist)
sizefile.close()
   