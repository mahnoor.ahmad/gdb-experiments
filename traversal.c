#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

#define ELEMENTS 3
#define WORD_SIZE 8U

#define is_word_aligned(POINTER) (((uintptr_t) (const void *)(POINTER) & (WORD_SIZE)) == 0)

int is_aligned(const void * pointer){
    uintptr_t number = (uintptr_t)pointer;
    return ( ( number % WORD_SIZE ) == 0 );
}

void checkptrs (unsigned int ** a, size_t size){

    unsigned int *traveler;
    int i = 0;
    for(i=0; i<size; i++){
        
        traveler = a[i];
        //printf("Arithmetic result %d and %p \n", is_word_aligned(traveler), traveler);
        if(traveler==NULL){
            printf("This pointer %p at %d is NULL\n", traveler, i);
        }   
        else{    
            if(! is_word_aligned(traveler))
                printf("This pointer %p at %d is not aligned on a word boundary\n", traveler, i);
        }
    }
}




int main()
{
    int *dummy = NULL;
    size_t size = ELEMENTS;
    unsigned int ** arr = (unsigned int **)malloc(sizeof(unsigned int *) * size);
    //dummy value population
    arr[2]=(unsigned int *) 96;
    arr[1]=(unsigned int *)0xdeadbeef; 
    //****

    checkptrs(arr, size);
    free(arr);
    return 0;

}

