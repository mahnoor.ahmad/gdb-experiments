class AllVars(gdb.Function):
    
    def __init__(self):
        super(AllVars, self).__init__("sizevars")

    def getsize(self, ttysize):
        gsize = ttysize.split("= ")[1]
        return gsize

    def invoke(self, size):
        import re, traceback
        regularexpression = "struct \w{1,}"
        varlist = []
        sizes = []
        varget = gdb.execute("info variables", False, True)
        varlist = re.findall(regularexpression, varget)
        varlist = list(dict.fromkeys(varlist))
        print(varlist)
        for i in range(len(varlist)):
            try:
                ttysize = gdb.execute("print sizeof("+varlist[i]+")", False, True)
            except Exception:
                traceback.print_exc()    
            sizefound = self.getsize(ttysize)
            sizes.append(sizefound)
            print(ttysize)        

        return 0
AllVars()