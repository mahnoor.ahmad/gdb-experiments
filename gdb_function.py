class MatchSize(gdb.Function):

    def __init__(self):
        super(MatchSize, self).__init__("sizematch")

    def getsize(self, ttysize):
        gsize = ttysize.split("= ")[1]
        gsize = gsize.replace("\n",  "")
        return gsize

    def invoke(self, size):
        frame = gdb.selected_frame()
        block = frame.block()
        names = []
        sizes = []
        while block:
            if(block.is_global):
                print()
                print('global vars')
            for symbol in block:
                if (symbol.is_argument or symbol.is_variable):
                    name = symbol.name
                    if not name in names:
                        print('{} = {}'.format(name, symbol.value(frame)))
                        names.append(name)
            block = block.superblock
        print("end list", names)
        for i in range(len(names)):
            ttysize = gdb.execute("print sizeof("+names[i]+")", from_tty=False, to_string=True)
            sizefound = self.getsize(ttysize)
            sizes.append(sizefound)
            print(ttysize)
            if sizefound==str(size):
                #gdb.write(names[i]+str(size), gdb.STDOUT)
                print(names[i]+" :"+str(size))
            #else:
                #print("These don't match", sizefound, " ", str(size))    

        return 0




MatchSize ()