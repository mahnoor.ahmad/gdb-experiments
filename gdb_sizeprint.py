from gdb.printing import PrettyPrinter, register_pretty_printer
import gdb

class StructDummy3Printer(object):
    
    def __init__(self, val):
        self.val = val

    def to_string(self):
        return "TODO: Implement" 

class CustomSizePrinter(PrettyPrinter):

    def __init__(self):
        super(CustomSizePrinter, self).__init__(
            "special struct size printer", []
        )

    def __call__(self, val):
        typename = gdb.types.get_basic_type(val.type).tag
        if typename is None:
            typename = val.type.name

        if typename == "dummy3":
            return StructDummy3Printer(val)

register_pretty_printer(None, CustomSizePrinter(), replace=True)   